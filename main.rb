#!/usr/bin/env ruby

require 'bundler'
puts "Bundler.settings[:gemfile] #{Bundler.settings[:gemfile]}"

require 'bundler/setup'

require "rack"
puts Rack.release
puts Gem.loaded_specs["rack"].version
