# Debug bundle

## prepare

```sh
gem install rack -v 1.2.8
gem install rack -v 2.2.3
gem list "^rack$"

bundle config set --local gemfile 'jh/Gemfile'
bundle config gemfile

bundle
```

## command

```sh
bundle exec ruby main.rb
```

## output

```text
Bundler.settings[:gemfile] /Users/cbd/Desktop/trail/debug-bundle/jh/Gemfile
1.2
1.2.8
```
